#!/usr/bin/env python3
import signal
import datetime
import yaml
import shlex
import re
import filecmp
import random
import time
from pathlib import Path, PurePath
from typing import Tuple
from plumbum import local, cli, colors
from plumbum import CommandNotFound
from plumbum.cmd import rm, timeout, mkdir, mv, wc, curl, bash

def get_random_seed():
    # Racket's random max value
    return random.randint(0, 4294967087 - 1)

class FuzzerCampaign(cli.Application):
    # todo: replace plumbum with more standard libraries

    # Interrupt handling
    def signal_handler(self, signum, frame):
        # Exit logic should be handled here, or else Plumbum will see the interrupt and exit without us.
        print("^C: Terminating now")
        # close() will flush the buffers
        self.log_file.close()
        self.bug_log_file.close()
        exit(42)

    # Possible configuration values. Set to 'None' initially to enforce order of
    # command-line -> config file -> defaults
    min_program_size = None
    timeout = None
    duration = None
    test_count = None
    fuzzer_binary = None
    output_dir = None
    config = None
    info_line_start = None
    info_line_end = None
    server_generator = None
    file_extension = None

    # plumbum command object
    generator = None

    # Detailed debugging
    detailed = None
    verbose = None

    # log files
    log_file = None
    bug_log_file = None

    @cli.switch(["-c", "--config"], str, mandatory=True)
    def load_config(self, config_file_path):
        """
        A file containing the configuration of the harness and compilers under test
        """
        # If this is running on a leaf node in Emulab, the config will be in /local/work/harness/configs/tmp/
        path = PurePath(config_file_path)
        with open(config_file_path, 'r') as f:
            self.config = yaml.load(f, Loader=yaml.BaseLoader)

        self.set_generator()
        # Don't check that all commands exist.
        # Ideally, it would be great, but we need a way to specify “this command
        # doesn't exist yet because it is the compiled output command”.
        #self.check_tools()
        self.set_server_mode()

    def set_generator(self):
        """
        Check that the random program generator exists and set the class variable
        :return: A plumbum command object bound with the configured command line options
        """
        generator = self.config["fuzzer"]["generator"]
        # split options list on spaces
        generator_options = self.config['fuzzer']['options']
        generator_options = [args for segments in generator_options for args in segments.split()]

        # Allow arguments to be passed in with the generator
        if len(generator.split()) > 1:
            generator_command = generator.split()
            generator = generator_command[0]
            later_generator_options = generator_options
            generator_options = generator_command[1:]
            generator_options.extend(later_generator_options)

        # Trust but verify
        if local.path(generator).exists():
            # Look locally first
            return local.get(local.path(generator))[generator_options]
        else:
            # The binary might be on the path
            try:
                self.generator = local.get(generator)[generator_options]
            except CommandNotFound:
                print(colors.warn | f"Random program generator binary '{generator}' not found.")
                exit(1)

        # File extension
        if self.config["fuzzer"].get("file-extension"):
            self.file_extension = self.config["fuzzer"]["file-extension"]
        else:
            self.file_extension = ".gen"

    def check_tools(self):
        """
        Check that all the compilers in the configuration file exist.
        """
        # Check that all the compilers exist
        tests = self.config['tests']
        for test in tests:
            for command in test['commands']:
                if len(command) != 1:
                    print(colors.warn | "Commands must be separated into a list. See the example configuration.")
                    exit(1)
                name, tool = next(iter(command.items()))
                if name == "fuzzer":
                    print(colors.warn | "The command name 'fuzzer' is reserved")
                    exit(1)
                tool = tool.split()[0]
                try:
                    local.get(tool)
                except CommandNotFound:
                    print(colors.warn | f"Command not found. Test name: '{test['name']}'. "
                                        f"Command name: '{name}'. Command: '{tool}'")
                    exit(1)

    def set_server_mode(self):
        if self.config.get("server"):
            self.server_generator = self.config['server'] in ["true", "True"]

    def get_config_options(self):
        """
        Gets options from the config file, respecting options already set from the command line arguments.
        """
        options = self.config["harness"]["options"]
        try:
            for option in options:
                flag = option.split()[0]
                if flag == "--min-program-size" and self.min_program_size is None:
                    arg = option.split()[1]
                    self.set_min_prog_size(int(arg))
                elif flag == "--info-lines" and self.info_line_start is None:
                    arg = option.split()[1]
                    self.set_info_lines(str(arg))
                elif flag == "-o" or flag == "--output-dir" and self.output_dir is None:
                    arg = option.split()[1]
                    self.set_output(str(arg))
                elif flag == "-t" or flag == "--timeout" and self.timeout is None:
                    arg = option.split()[1]
                    self.set_timeout(int(arg))
                elif flag == "-d" or flag == "--duration" and self.duration is None:
                    arg = option.split()[1]
                    self.set_duration(int(arg))
                elif flag == "--verbose" and self.verbose is None:
                    self.verbose = True
        except IndexError:
            print(colors.warn | "Expected option argument not provided. Exiting")
            exit(3)

    def get_default_options(self):
        """
        Set any remaining options that weren't specified on either the command line or in the config file.
        """
        if self.min_program_size is None:
            self.min_program_size = 1
        if self.timeout is None:
            self.timeout = 30
        if self.test_count is None:
            self.test_count = 10
        if self.output_dir is None:
            self.output_dir = local.cwd / "output"
        if self.info_line_start is None:
            self.info_line_start = 0
        if self.info_line_end is None:
            self.info_line_end = 10
        if self.server_generator is None:
            self.server_generator = False
        if self.detailed is None:
            self.detailed = False
        if self.verbose is None:
            self.verbose = False

    @cli.switch("--min-program-size", int)
    def set_min_prog_size(self, size):
        """
        Programs shorter than this many bytes are too boring to test.
        Default: 1.
        """
        self.min_program_size = size

    @cli.switch("--info-lines", str)
    def set_info_lines(self, raw_lines: str):
        """
        Specifies the lines to save from the generated program. These lines typically contain information like the
        random seed, the generator version, the runtime version, etc...
        The format must be of the form <'('|'['>start,end<')'|']'>, with '(' or ')' being exclusive, and '[' or ']'
        being inclusive. For example: "--info-lines [2,5)".
        """
        error_string = "Bounds must be of the form [first,last] or (first,last), with square braces for\n" \
                       "inclusive bounds and parentheses for exclusive bounds"
        if ',' not in raw_lines:
            raise ValueError(error_string)
        raw_lo, raw_hi = raw_lines.split(',')
        if not raw_lo or not raw_hi:
            raise ValueError(error_string)
        lo_incl = '['
        lo_excl = '('
        hi_incl = ']'
        hi_excl = ')'
        lower_bounds = {lo_incl, lo_excl}
        upper_bounds = {hi_incl, hi_excl}
        lo_bound = raw_lo[0]
        hi_bound = raw_hi[-1]
        if lo_bound not in lower_bounds or hi_bound not in upper_bounds:
            raise ValueError("Bounds must be of the form [first,last] or (first,last), or a combination of square\n"
                             "and round braces, with square braces for inclusive bounds and parentheses for exclusive\n"
                             "bounds")
        lo_inclusive = lo_bound == lo_incl
        hi_inclusive = hi_bound == hi_incl
        lo_num = int(raw_lo[1:].strip()) - 1  # Adjust the numbers to account for indexing. Users pass in
        hi_num = int(raw_hi[:-1].strip()) - 1  # 1-indexed values, but Python will expect 0-indexed values.
        if not lo_inclusive:
            lo_num += 1
        if hi_inclusive:
            hi_num += 1
        if lo_num > hi_num:
            raise ValueError("The info line range cannot be negative or zero")
        self.info_line_start = lo_num
        self.info_line_end = hi_num

    @cli.switch(["-n", "--count"], int, excludes=["-d", "--duration"])
    def set_count(self, count):
        """
        The number of programs to generate and test. This option is most likely used by the Ansible wrapper. It
        corresponds to the batch_size option in the configuration file. Exclusive with the -d/--duration flag.
        """
        self.test_count = count

    @cli.switch(["-o", "--output-dir"], str)
    def set_output(self, output_dir):
        """
        Sets the output directory name.
        """
        self.output_dir = local.path(output_dir)

    @cli.switch(["-t", "--timeout"], int)
    def set_timeout(self, timeout):
        """
        Sets the timeout for all commands run as a part of a test.
        """
        self.timeout = timeout

    @cli.switch(["-d", "--duration"], int, excludes=["-n", "--count"])
    def set_duration(self, duration):
        """
        Sets the experiment to run for a specified duration (in seconds). Exclusive with the -n/--count flag.
        """
        self.duration = duration
        # We need to set the count here, since the flag is exclusive
        self.test_count = 1

    @cli.switch(["--detailed"])
    def set_detailed(self):
        """
        Switches the harness into a debugger for a previously generated program.
        This mode will query the user for the info lines of the program they
        wish to debug, and will print all std-out and std-err to the console for
        each command in a test.
        """
        self.detailed = True

    @cli.switch(["--verbose"])
    def set_verbose(self):
        """
        Tells the harness to print results verbosely. Instead of lumping outputs into
        groups for display in the log files, the harness will instead print the whole
        output to the log. Be careful with this option, as leaving it on for a full
        testing campaign can fill up the testing machines rather quickly.
        Keep in mind that extra newlines in the output can confuse the log parser.
        """
        self.verbose = True

    def generate(self, source_file, seed=None):
        """
        Generates a random program, subject to the minimum program size constraint.
        It will generate logs about rejected programs. If the entire log is filled
        with rejected programs, consider lowering the minimum size.

        WARNING: If the minimum program size is too large, this method can loop infinitely.

        :param source_file: the filename of the generated program.
        :return: the number of crashes before generating a valid program
        """
        local_fuzzer_errors = 0
        # Just run ONCE
        rm("-f", source_file)
        def error_print(is_crash, std_out, std_err):
            with open(source_file, mode="r") as f:
                error_info = f.read()
                if is_crash:
                    message = [f"[!!!!] Xsmith crash:\t\tseed: {seed} \t{datetime.datetime.utcnow().isoformat()}\n",
                               f"          std_out: {std_out}\n",
                               f"          std_err: {std_err}\n",
                               f"          generated file:\n"]
                else:
                    message = [f"[!!!!] Xsmith error:\t\tseed: {seed} \t{datetime.datetime.utcnow().isoformat()}\n",
                               f"          std_out: {std_out}\n",
                               f"          std_err: {std_err}\n",
                               f"          generated file:\n"]

                print(*message, error_info, sep='', file=self.log_file)
                print(*message, error_info, sep='', file=self.bug_log_file)

        def timeout_print():
            with open(source_file, mode="r") as f:
                error_info = f.read()
                message = f"[!!!!] Generator timeout:\t\tseed: {seed} \t{datetime.datetime.utcnow().isoformat()}  "
                print(message, file=self.log_file)

        if self.server_generator:
            # If you get warnings on these next few lines: They're not warnings, just plumbum magic
            (curl["localhost:8080"] > source_file)()
        else:
            if seed:
                return_code, std_out, std_err = (self.generator["--seed", str(seed)] > source_file).run(retcode=None)
            else:
                return_code, std_out, std_err = (self.generator > source_file).run(retcode=None)

            if return_code != 0:  # Generator returned non-zero
                local_fuzzer_errors += 1
                error_print(True, std_out, std_err)
                if self.detailed:
                    exit(1)
                else:
                    return 1

        # Check for generation errors in the file itself.
        # The server mode can't signal an error by return code.
        with open(source_file, mode="r") as f:
            text = f.read()
            if "Generation failed because timeout was exceeded." in text:
                timeout_print()
                return 1
            if "!!! Xsmith Error !!!" in text:
                local_fuzzer_errors += 1
                error_print(False, text, " ")
                return 1

        program_size = int((wc["-c"] < source_file)())  # input redirection

        if program_size > self.min_program_size:
            return local_fuzzer_errors
        else:
            # log the rejected program
            log_output = [f"[----] Program rejected:\t\tseed: {seed} {datetime.datetime.utcnow().isoformat()}\n",
                          f"       Generated size: {program_size}\n",
                          f"       Minimum size: {self.min_program_size}\n\n"]
            print(*log_output, sep='', file=self.log_file)
            return 1

    def run_tests(self):
        fuzzer_errors = 0
        crashes = 0
        hangs = 0
        inconsistent_hangs = 0
        wrong_code = 0

        mkdir("-p", self.output_dir)
        self.log_file = open(self.output_dir / "full-log.txt", "a")
        self.bug_log_file = open(self.output_dir / "bug-log.txt", "a")

        header_lvl_0 = "[----]"
        header_lvl_1 = "[-!--]"
        header_lvl_2 = "[-!!-]"
        header_lvl_3 = "[-!!!]"
        header_lvl_4 = "[!!!!]"
        dollar_pattern = re.compile(r"\$(?!\$)")  # dollar sign not followed by another
        reference_pattern = re.compile(r'(?<!\$)\$([a-zA-Z0-9._-]+)')
        subshell_pattern = re.compile(r'\$\((.*)\)')

        for i in range(self.test_count):
            test_subdir = self.output_dir / "test_temp"
            source_file = self.output_dir / "fuzzer-out" + self.file_extension
            seed = get_random_seed()
            generation_error = self.generate(source_file, seed)
            fuzzer_errors += generation_error
            if generation_error != 0:
                # if there was an error, there's nothing more to do in this current iteration of the loop.
                continue

            # Save the header, since it has info we need to put in the log file
            with open(source_file, 'r') as f:
                info_lines = f.readlines()
            info_lines_orig = info_lines
            info_lines = info_lines[self.info_line_start:self.info_line_end]
            info_lines[-1] = info_lines[-1].rstrip()
            info_lines = '       '.join(info_lines)
            if "!!! Xsmith Error !!!" in info_lines:
                info_lines = info_lines_orig[self.info_line_start + 3 : self.info_line_end + 3]
                info_lines = '       '.join(["!!! Xsmith Error !!!"]+info_lines)
                log_output = [f"{header_lvl_4} Generation error:\t\tseed: {seed} {datetime.datetime.utcnow().isoformat()}\n",
                              f"       {info_lines}\n\n"]
                print(*log_output, sep='', file=self.log_file)
                print(*log_output, sep='', file=self.bug_log_file)
                continue

            tests = self.config['tests']
            normal_results = []
            crash_results = []
            hang_results = []
            for test_index, test in enumerate(tests):
                test_name = test["name"]
                mkdir("-p", test_subdir)
                references = set()  # Make an empty set
                for command in test['commands']:
                    command_name, tool = next(iter(command.items()))
                    tool = tool.replace("$fuzzer-output", source_file)

                    # Check the references
                    for word in tool.split():
                        if word.startswith("$") and not word.startswith("$$"):
                            reference = word[1:].split('.', 1)
                            if len(reference) != 2 or not (reference[1] == "stdout" or reference[1] == "stderr"):
                                print(colors.warn | f"Not a valid reference: '{word}' in '{test_name}'.\n"
                                                    f"References must follow the form '$<previous command name>(.stdout|.stderr)'.\n"
                                                    f"If trying to use an environment variable, like $HOME, escape with $$: $$HOME.\n")
                                exit(1)
                            if reference not in references:
                                print(colors.warn | f"Not a valid reference: '{word}' in '{test_name}'.\n"
                                                    f"References must not precede the step they refer to.\n")
                                exit(1)

                    # Replace reference with path to file
                    tool = re.sub(reference_pattern, f"{test_subdir}/\g<1>", tool)
                    # Unnest all dollar signs
                    tool = re.sub(dollar_pattern, '', tool)
                    # Expand environment variables
                    tool = local.env.expand(tool)
                    # Check for a subshell command
                    subshell = re.search(subshell_pattern, tool)
                    if subshell:
                        # Extract the command and run it
                        subshell_command = subshell.expand(r'\g<1>')
                        _, subshell_out, _ = bash['-c', subshell_command].run(cwd=test_subdir, retcode=None)
                        # Replace the subshell command in the tool with the result
                        tool = re.sub(subshell_pattern, subshell_out.strip(), tool)

                    # Run the command, writing output to the files
                    run_args = [self.timeout]
                    run_args.extend(shlex.split(tool))
                    run_chain = timeout[run_args]
                    # Defaults: set return code, stdout, and stderr to empty/0
                    return_code, std_out, std_err = run_chain.run(cwd=test_subdir, retcode=None)
                    references.add(command_name)

                    if return_code == 127:  # error - command not found.  This means there is a configuration error.
                        print(colors.warn | f"Configuration error, command not found for test: {test_name}.\n {std_err}")
                        exit(1)
                    if return_code == 124:  # hang
                        normal_results.append(None)
                        crash_results.append(None)
                        hang_results.append(f"{test_name} -> {command_name}")
                        hangs += 1
                        break  # start the next test
                    if return_code != 0:  # crash
                        normal_results.append(None)
                        crash_results.append(f"{test_name} -> {command_name}")
                        hang_results.append(None)
                        crashes += 1
                        if self.verbose:
                            with open(self.output_dir / f"test-{test_index}.crash.output", mode='w') as f:
                                f.write(std_out)
                                f.write(std_err)
                        break
                    else:  # normal
                        with open(test_subdir / f"{command_name}.stdout", mode='w') as f:
                            f.write(std_out)
                            f.flush()
                        with open(test_subdir / f"{command_name}.stderr", mode='w') as f:
                            f.write(std_err)
                            f.flush

                        # Save output if last step
                        if command == test['commands'][-1]:
                            with open(self.output_dir / f"test-{test_index}.output", mode='w') as f:
                                f.write(std_out)
                            normal_results.append(test_name)
                            crash_results.append(None)
                            hang_results.append(None)
                # End of command iteration
                rm("-rf", test_subdir)  # Wipe the output directory for the next test
            # End of test iteration

            # Tabulate the test results
            # First, report any crashes
            if not (None in crash_results):
                # All crashed, certainly the generated program is bad (divide by 0 is a common example)
                log_output = [f"{header_lvl_2} Bad program, all tests crashed:\tseed: {seed} {datetime.datetime.utcnow().isoformat()}\n",
                              f"       {info_lines}\n"]
                if self.verbose:
                    for test_index, test_name in enumerate(crash_results):
                        with open(self.output_dir / f"test-{test_index}.crash.output", mode='r') as f:
                            crash_output = f.read()
                        log_output.append(f"       {test_name} ==> {crash_output}\n")

                log_output.append("\n") # split up the log entries

                print(*log_output, sep='', file=self.log_file)
                print(*log_output, sep='', file=self.bug_log_file)
            elif any(crash_results):
                log_output = [f"{header_lvl_2} Test crash:\tseed: {seed} {datetime.datetime.utcnow().isoformat()}\n",
                              f"       {info_lines}\n"]
                for test_index, test_name, result in zip(range(len(normal_results)), normal_results, crash_results):
                    # Crash
                    if result is not None:
                        if self.verbose:
                            with open(self.output_dir / f"test-{test_index}.crash.output", mode='r') as f:
                                crash_output = f.read()
                            log_output.append(f"       {result}  (!)==> {crash_output}\n")
                        else:
                            log_output.append(f"       {result}: crash\n")
                    # Normal
                    else:
                        if self.verbose:
                            with open(self.output_dir / f"test-{test_index}.output", mode='r') as f:
                                normal_output = f.read()
                            log_output.append(f"       {test_name} ==> {normal_output}\n")
                        else:
                            log_output.append(f"       {test_name}: ---\n")

                # one extra newline to split up the log entries
                log_output.append("\n")

                print(*log_output, sep='', file=self.log_file)
                print(*log_output, sep='', file=self.bug_log_file)
            # Check for all hangs
            elif all(hang_results):
                log_output = [f"{header_lvl_1} All tests timed out:\t\tseed: {seed} {datetime.datetime.utcnow().isoformat()}\n"]
                for result in hang_results:
                    log_output.append(f"       Timeout at: {result}\n")
                log_output.append(f"       {info_lines}\n\n")
                print(*log_output, sep='', file=self.log_file)
                print(*log_output, sep='', file=self.bug_log_file)
            # Check for inconsistent hangs
            elif any(hang_results) and any(normal_results):
                log_output = [f"{header_lvl_3} Inconsistent hang:\t\tseed: {seed} {datetime.datetime.utcnow().isoformat()}\n"]
                normal_and_hangs = zip(normal_results, hang_results)
                for normal_result, hang_result in normal_and_hangs:
                    if normal_result:
                        log_output.append(f"       Normal execution: {normal_result}\n")
                    if hang_result:
                        log_output.append(f"       Timeout at: {hang_result}\n")
                log_output.append(f"       {info_lines}\n\n")
                print(*log_output, sep='', file=self.log_file)
                print(*log_output, sep='', file=self.bug_log_file)
                inconsistent_hangs += 1

            # Check for wrong outputs
            elif any(normal_results):
                output_bins = []
                same_outputs = True
                maximum_bin = 'A'
                current_bin = maximum_bin
                first_output = None
                for test_index, result in enumerate(normal_results):
                    output_bins.append(None)  # Default none

                    if result:  # Remember, result could be None
                        if first_output is None:
                            first_output = test_index
                            output_bins[test_index] = current_bin
                            continue

                        # Loop over the previous tests, looking for a matching output.
                        matching_bin = None
                        for previous_test_index in range(test_index):
                            outputs_match = filecmp.cmp(self.output_dir / f"test-{test_index}.output",
                                                        self.output_dir / f"test-{previous_test_index}.output")
                            if outputs_match:
                                matching_bin = output_bins[previous_test_index]
                                break
                        # Update the current test's bin.
                        if matching_bin is not None:
                            # We found a match, so just use that bin.
                            current_bin = matching_bin
                        else:
                            # No match was found, so we increment the maximum bin and set the current bin to that
                            maximum_bin = chr(ord(maximum_bin) + 1)
                            current_bin = maximum_bin

                            # Set sentinel for log output
                            same_outputs = False
                        # Record this test's bin name.
                        output_bins[test_index] = current_bin

                if same_outputs:
                    log_output = [f"{header_lvl_0} Test output matches:\t\tseed: {seed} {datetime.datetime.utcnow().isoformat()}\n",
                                  f"       {info_lines}\n\n"]
                    print(*log_output, sep='', file=self.log_file)
                else:
                    bin_readout = ""
                    for test_index, test_name, bin_label in zip(range(len(normal_results)), normal_results, output_bins):
                        if test_name is not None:
                            if self.verbose:
                                with open(self.output_dir / f"test-{test_index}.output", mode='r') as f:
                                    test_output = f.read()
                                bin_readout += f"       - {test_name} ==> {test_output}\n"
                            else:
                                bin_readout += f"       - {test_name}: {bin_label}\n"

                    log_output = '\n'.join([f"{header_lvl_4} Test output does not match:\t\tseed: {seed} {datetime.datetime.utcnow().isoformat()}",
                                            f"       {info_lines}",
                                            f"       Test outputs:",
                                            f"{bin_readout}\n"])
                    print(log_output, file=self.log_file)
                    print(log_output, file=self.bug_log_file)
                    wrong_code += 1
            info_string = f"[INFO]  Progress: {i + 1}\t Crashes: {crashes}\t " \
                          f"Hangs: {hangs}\t Wrong code: {wrong_code}\t" \
                          f"Fuzzer Errors: {fuzzer_errors}"
            print(info_string)

            # Clean up output files
            for test_index, result in enumerate(normal_results):
                if result is not None:
                    rm(self.output_dir / f"test-{test_index}.output")
            rm(source_file)

            # Flush the log files to disk after each test
            self.log_file.flush()
            self.bug_log_file.flush()
        # End batch iteration

        self.log_file.close()
        self.bug_log_file.close()

    def run_test_detailed(self):
        # ask the user for the input lines
        print("Copy-paste the info lines of the program you have detailed info of here followed by a blank line:\n")
        info_lines = []
        while True:
            line = input()
            if line:
                info_lines.append(line)
            else:
                break

        # parse the data we need: just the seed. The config file has the other options
        seed = 0
        for line in info_lines:
            if ";; Seed: " in line:
                seed = line.split()[-1]
                break
            elif "--seed" in line:
                options_line = line.split()
                seed_index = options_line.index("--seed") + 1
                seed = options_line[seed_index]
                break

        print(f"Seed: {seed}")

        mkdir("-p", self.output_dir)

        dollar_pattern = re.compile(r"\$(?!\$)")  # dollar sign not followed by another
        reference_pattern = re.compile(r'(?<!\$)\$([a-zA-Z0-9._-]+)')

        test_subdir = self.output_dir / "detailed-test_temp"
        source_file = self.output_dir / "detailed-fuzzer-out" + self.file_extension
        self.generate(source_file, seed)

        tests = self.config['tests']
        for test_index, test in enumerate(tests):
            test_name = test["name"]
            mkdir("-p", test_subdir)
            references = set()
            for command in test['commands']:
                command_name, tool = next(iter(command.items()))
                tool = tool.replace("$fuzzer-output", source_file)

                # Check the references
                for word in tool.split():
                    if word.startswith("$") and not word.startswith("$$"):
                        reference, direction = word[1:].split('.', 1)
                        if not (direction == "stdout" or direction == "stderr"):
                            print(colors.warn | f"Not a valid reference: '{word}' in '{test_name}'.\n"
                                                f"References must follow the form '$<previous command name>(.stdout|.stderr)'.\n"
                                                f"If trying to use an environment variable, like $HOME, escape with $$: $$HOME.\n")
                            exit(1)
                        if reference not in references:
                            print(colors.warn | f"Not a valid reference: '{word}' in '{test_name}'.\n"
                                                f"References must not precede the step they refer to.\n")
                            exit(1)

                # Replace reference with path to file
                tool = re.sub(reference_pattern, f"{test_subdir}/\g<1>", tool)
                # Unnest all dollar signs
                tool = re.sub(dollar_pattern, '', tool)
                # Expand environment variables
                tool = local.env.expand(tool)

                # Run the command, writing output to the files
                run_args = [self.timeout]
                run_args.extend(shlex.split(tool))
                run_chain = timeout[run_args]
                return_code, std_out, std_err = run_chain.run(retcode=None)
                references.add(command_name)
                # Save the stdout and stderr for printing
                with open(test_subdir / f"{command_name}.stdout", mode='w') as f:
                    f.write(std_out)
                with open(test_subdir / f"{command_name}.stderr", mode='w') as f:
                    f.write(std_err)

                detailed_output = f"=======================================================\n" \
                                  f"Output: {test_name}, {command_name}\n" \
                                  f"Return code: {return_code}\n" \
                                  f"-------------------------------------------------------\n" \
                                  f"STDOUT\n" \
                                  f"-------------------------------------------------------\n" \
                                  f"{std_out}\n" \
                                  f"-------------------------------------------------------\n" \
                                  f"STDERR\n" \
                                  f"-------------------------------------------------------\n" \
                                  f"{std_err}\n" \
                                  f"-------------------------------------------------------\n\n"
                print(detailed_output)

                if return_code != 0:
                    break

            # End of command iteration
            rm("-rf", test_subdir)  # Wipe the output directory for the next test
        # End of test iteration

    def main(self):
        """
        Main method. Called from plumbum after command line arguments have been parsed
        """
        # Register signal handler
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

        # Command line options have been set when this point is reached
        # Get options from config file
        self.get_config_options()
        # Get remaining defaults
        self.get_default_options()

        if self.detailed:
            self.server_generator = False
            self.run_test_detailed()
        else:
            if self.duration:  # Duration-based loop
                start_time = time.time()
                while True:
                    self.run_tests()
                    elapsed_time = time.time() - start_time
                    if elapsed_time > self.duration:
                        break
            else:  # Count-based loop
                self.run_tests()


if __name__ == "__main__":
    FuzzerCampaign.run()
