import os
import subprocess
import tkinter as tk
import threading
from tkinter import ttk, messagebox
import requests

class Application(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Racket Server GUI")
        self.geometry("600x400")
        self.create_widgets()

    def create_widgets(self):
        self.label = tk.Label(self, text="Select a .rkt file:")
        self.label.pack()

        self.files_list = ttk.Combobox(self)
        self.files_list['values'] = [f for f in os.listdir('.') if f.endswith('.rkt')]
        self.files_list.current(0)
        self.files_list.pack()

        tk.Label(self, text="Max depth", font=('Calibri 10')).pack()
        self.depth = tk.Entry(self, text="max-depth")
        self.depth.pack()

        tk.Label(self, text="Seed", font=('Calibri 10')).pack()
        self.seed = tk.Entry(self, text="seed")
        self.seed.pack()

        self.error_render = tk.BooleanVar()
        self.check1 = tk.Checkbutton(self, text="render-on-error", variable=self.error_render)
        self.check1.pack()

        self.print_debug = tk.BooleanVar()
        self.check2 = tk.Checkbutton(self, text="print-debug", variable=self.print_debug)
        self.check2.pack()

        self.print_sexps = tk.BooleanVar()
        self.check3 = tk.Checkbutton(self, text="print-sexps", variable=self.print_sexps)
        self.check3.pack()

        self.show_sexp_fields = tk.BooleanVar()
        self.check4 = tk.Checkbutton(self, text="show-sexp-fields", variable=self.show_sexp_fields)
        self.check4.pack()

        self.button = tk.Button(self, text="Start Server", command=self.start_server)
        self.button.pack()

    def start_server(self):
        options = []
        if self.depth.get() != "":
            options.extend(["--max-depth", self.depth.get()])
        if self.error_render.get():
            options.extend(["--render-on-error", "true"])
        if self.error_render.get():
            options.extend(["--print-debug", "true"])
        if self.seed.get() != "":
            options.extend(["--seed", self.seed.get()])
        if self.print_sexps.get():
            options.extend(["--s-exp-print-override", "true"])
        if self.show_sexp_fields.get():
            options.extend(["--s-exp-show-base-fields", "t"])

        cmd = ["racket", self.files_list.get()] + options + ["--server", "true"]

        self.process = subprocess.Popen(cmd)
        self.new_window()

    def new_window(self):
        self.destroy()
        ResultWindow(self.process).mainloop()
        

class ResultWindow(tk.Tk):
    def __init__(self, process):
        super().__init__()
        self.title("Server Result")
        self.geometry("600x400")
        self.create_widgets()
        self.process = process
        self.protocol("WM_DELETE_WINDOW", self.on_closing)

    def create_widgets(self):
        self.button = tk.Button(self, text="Generate Program", command=self.get_result)
        self.button.pack()

        self.button2 = tk.Button(self, text="Pipe to runGHC", command=self.pipe_to_runGHC)
        self.button2.pack()

        self.text = tk.Text(self)
        self.text.pack()

    def get_result(self):
        try:
            response = requests.get("http://localhost:8080")
            self.text.delete(1.0, tk.END)
            self.text.insert(tk.END, response.text)
        except requests.exceptions.RequestException as e:
            messagebox.showerror("Error", str(e))

    def pipe_to_runGHC(self):
        try:
            program = self.text.get(1.0, tk.END)
            with open('temp.hs', 'w') as f:
                f.write(program)
            cmd = ["runghc", "temp.hs", "--XExtendedDefaultRules"]
            subprocess.run(cmd)
        except requests.exceptions.RequestException as e:
            messagebox.showerror("Error", str(e))

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self.process.kill()
            self.destroy()


if __name__ == "__main__":
    app = Application()
    app.mainloop()
